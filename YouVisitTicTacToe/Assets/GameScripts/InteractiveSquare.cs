﻿using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class InteractiveSquare : MonoBehaviour, IInteractive
{
	private Interaction interact;
	private int index;

	private void Reset()
	{
		BoxCollider2D col = this.GetComponent<BoxCollider2D>();
		col.isTrigger = true;
	}

	private void OnMouseDown()
	{
		if (interact != null)
		{
			interact.Invoke(index);
		}
	}

	#region IInteractive implementation

	public void SetDimensions(float width, float height)
	{
		BoxCollider2D col = this.GetComponent<BoxCollider2D>();
		col.size = Vector2.one * width;
		col.offset = new Vector2(width / 2f, -width / 2f);
	}

	public void SetPosition(Vector2 position)
	{
		transform.position = position;
	}

	public Interaction InteractCallback
	{
		set
		{
			interact -= value;
			interact += value;
		}
	}

	public int Index
	{
		set
		{
			index = value;
		}
	}

	#endregion
}
