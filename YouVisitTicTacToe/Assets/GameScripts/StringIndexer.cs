﻿using UnityEngine;

[RequireComponent(typeof(TextMesh))]
public class StringIndexer : MonoBehaviour, IIndexer
{
    [SerializeField]
    private string[] strings;

    public void SelectIndex(int index)
    {
        if (index >= 0 && index < strings.Length)
        {
            GetComponent<TextMesh>().text = strings[index];
        }
        else
        {
            Debug.LogError("Index was out of range for indexer, received " + index +
                " expected [0," + strings.Length + ")", this);
        }
    }
}
