﻿using UnityEngine;
using UnityEngine.UI;

public class TicTacToeGame : MonoBehaviour
{
    [SerializeField]
    private UserInteractionGenerator interaction;
    [SerializeField]
    private GridPresentation presentation;
    [SerializeField]
    private MatchLineGridEvaluator evaluation;

    [SerializeField]
    private Text messageText;
    [SerializeField]
    private GameObject newGameButton;

	private void Start()
    {
        newGameButton.SetActive(false);
        interaction.Init(evaluation.Select);
        presentation.GenerateInteractionPoint = interaction.GenerateInterationPoint;
        presentation.Init(evaluation.Parameters);
        evaluation.RegisterForGridUpdates = presentation.UpdateGameGrid;
        evaluation.RegisterForTurnUpdates = OnNewPlayerTurn;
        evaluation.RegisterForWinUpdates = OnPlayerWin;
        evaluation.Init();
    }

    private void OnNewPlayerTurn(int index)
    {
        messageText.text = "Player " + (index + 1) + "'s Turn";
    }

    private void OnPlayerWin(int index)
    {
        if (index == -1)
        {
            messageText.text = "Stalemate!";
        }
        else
        {
            messageText.text = "Player " + (index + 1) + " Wins!";
        }
        newGameButton.SetActive(true);
    }

    public void OnNewGameButtonPress()
    {
        evaluation.Clear();
        newGameButton.SetActive(false);
    }
}
