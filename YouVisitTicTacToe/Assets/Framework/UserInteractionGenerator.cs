﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class UserInteractionGenerator
{
    [SerializeField]
    private GameObject interactionPrefab;

    private int indexGenerator;
    private List<GameObject> interactivePoints;
    private GameObject interactionParent;
    private Interaction gameInteraction;

    /// <summary>
    /// explicit init since this object is serialized
    /// </summary>
    public void Init(Interaction gameInteractionCallback)
    {
        indexGenerator = 0;
        gameInteraction -= gameInteractionCallback;
        gameInteraction += gameInteractionCallback;
        interactivePoints = new List<GameObject>();
        if (interactionParent == null)
        {
            interactionParent = new GameObject("Interaction");
        }
    }

    /// <summary>
    /// make an interacton point
    /// </summary>
    /// <param name="position"></param>
    /// <param name="size"></param>
    public void GenerateInterationPoint(Vector2 position, Vector2 size)
    {
        if (interactionPrefab == null)
        {
            Debug.LogError("Interaction prefab is not set in editor");
        }
        else
        {
            GameObject prefabCopy = Object.Instantiate(interactionPrefab);
            prefabCopy.name = "Interactive";
            IInteractive interactive = prefabCopy.GetComponent<IInteractive>();
            if (interactive == null)
            {
                Object.Destroy(prefabCopy);
                Debug.LogError("Interaction prefab is missing the IInteractive component");
            }
            else
            {
                interactive.Index = indexGenerator++;
                interactive.SetPosition(position);
                interactive.SetDimensions(size.x, size.y);
                interactive.InteractCallback = gameInteraction;
                prefabCopy.transform.SetParent(interactionParent.transform);
                interactivePoints.Add(prefabCopy);
            }
        }
    }
    
    /// <summary>
    /// Clear all interactive points
    /// </summary>
    public void ClearInteractionPoints()
    {
        while (interactivePoints.Count > 0)
        {
            Object.Destroy(interactivePoints[0]);
            interactivePoints.RemoveAt(0);
        }
    }
}
