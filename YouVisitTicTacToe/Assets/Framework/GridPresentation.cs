﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class GridPresentation : IPresentationGenerator
{
	[SerializeField]
	private GameObject linePrefab;
    [SerializeField]
    private GameObject gameTilePrefab;

	private List<LineRenderer> lines;
    private GameObject[] tiles;
	private DimensionPass makeInteractionPoint;
    private GameParameters parameters;
    private GameObject presentationParent;

	private const float LINE_THICKNESS = .1f;

    /// <summary>
    /// initialize explicitly since this object is serialized
    /// </summary>
    /// <param name="param"></param>
	public void Init(GameParameters param)
	{
        parameters = param;
		lines = new List<LineRenderer>();
        tiles = new GameObject[(int)Mathf.Pow(parameters.gridSize, 2)];
        if (presentationParent == null)
        {
            presentationParent = new GameObject("GridPresentation");
        }
        GeneratePresentation(parameters);
    }

    /// <summary>
    /// Callback for updating the game grid according to game logic
    /// </summary>
    /// <param name="gridUpdate"></param>
    public void UpdateGameGrid(GridUpdate gridUpdate)
    {
        if (gridUpdate.move == GridUpdate.Blank)
        {
            int tileIndex = gridUpdate.y * parameters.gridSize + gridUpdate.x;
            if (tileIndex >= 0 && tileIndex < tiles.Length)
            {
                if (tiles[tileIndex] != null)
                {
                    Object.Destroy(tiles[tileIndex]);
                }
            }
        }
        else
        {
            if (gameTilePrefab == null)
            {
                Debug.LogError("Game tile prefab is not set in editor");
            }
            else
            {
                GameObject prefabCopy = Object.Instantiate(gameTilePrefab);
                prefabCopy.name = "Tile";
                IIndexer indexer = prefabCopy.GetComponentInChildren<IIndexer>();
                if (indexer == null)
                {
                    Object.Destroy(prefabCopy);
                    Debug.LogError("Game tile prefab does not have an IIndexer component");
                }
                else
                {
                    indexer.SelectIndex(gridUpdate.move);
                    int tileIndex = gridUpdate.y * parameters.gridSize + gridUpdate.x;
                    if (tileIndex >= 0 && tileIndex < tiles.Length)
                    {
                        if (tiles[tileIndex] != null)
                        {
                            Object.Destroy(tiles[tileIndex]);
                        }
                        float horizontalLineSpacing = parameters.width / parameters.gridSize;
                        float verticalLineSpacing = parameters.height / parameters.gridSize;
                        float x = parameters.anchorPoint.x + (horizontalLineSpacing * gridUpdate.y);
                        float y = parameters.anchorPoint.y - (verticalLineSpacing * gridUpdate.x);
                        prefabCopy.transform.position = new Vector2(x, y);
                        prefabCopy.transform.SetParent(presentationParent.transform);
                        tiles[tileIndex] = prefabCopy;
                    }
                    else
                    {
                        Object.Destroy(prefabCopy);
                        Debug.LogError("Target tile index out of range, received "
                            + tileIndex + " expected [0," + tiles.Length + ")");
                    }
                }
            }
        }
    }

	private void PlaceLine(Vector2 start, Vector2 end)
	{
		if (linePrefab == null)
		{
			Debug.LogError("Line prefab is not set, set this in the editor");
		}
		else
		{
			GameObject prefabCopy = Object.Instantiate(linePrefab);
			prefabCopy.name = "Line";
			LineRenderer lineRenderer = prefabCopy.GetComponent<LineRenderer>();
			if (lineRenderer == null)
			{
				Object.Destroy(prefabCopy);
				Debug.LogError("Line prefab is missing a LineRenderer component");
			}
			else
			{
				lineRenderer.SetPosition(0, start);
				lineRenderer.SetPosition(1, end);
                prefabCopy.transform.SetParent(presentationParent.transform);
				lines.Add(lineRenderer);
			}
		}
	}

	#region IPresentationGenerator implementation

	/// <summary>
	/// Generate all presentation for this grid
	/// </summary>
	/// <param name="parameters">the Parameter field is the square root for the number of gris squares</param>
	public void GeneratePresentation(GameParameters parameters)
	{
		float horizontalLineSpacing = parameters.width / parameters.gridSize;
		float verticalLineSpacing = parameters.height / parameters.gridSize;
		for (int i = 1; i < parameters.gridSize; ++i)
		{
			float x = parameters.anchorPoint.x + (horizontalLineSpacing * i);
			float y = parameters.anchorPoint.y - (verticalLineSpacing * i);
			PlaceLine(new Vector2(x, parameters.anchorPoint.y),
				new Vector2(x, parameters.anchorPoint.y - parameters.height));
			PlaceLine(new Vector2(parameters.anchorPoint.x, y),
				new Vector2(parameters.anchorPoint.x + parameters.width, y));
		}
		if (makeInteractionPoint != null)
		{
			for (int i = 0; i < parameters.gridSize; ++i)
			{
				float y = parameters.anchorPoint.y - (horizontalLineSpacing * i);
				for (int j = 0; j < parameters.gridSize; ++j)
				{
					float x = parameters.anchorPoint.x + (verticalLineSpacing * j);
					float length = parameters.width / parameters.gridSize - LINE_THICKNESS;
					makeInteractionPoint(new Vector2(x, y), new Vector2(length, length));
				}
			}
		}
	}

    /// <summary>
    /// Remove everything
    /// </summary>
	public void ClearPresentation()
	{
		while (lines.Count > 0)
		{
			Object.Destroy(lines[0].gameObject);
			lines.RemoveAt(0);
		}
	}

    /// <summary>
    /// Set the callback for when we want to place an interaction point
    /// </summary>
	public DimensionPass GenerateInteractionPoint
	{
		set
		{
			makeInteractionPoint -= value;
			makeInteractionPoint += value;
		}
	}

	#endregion
}
