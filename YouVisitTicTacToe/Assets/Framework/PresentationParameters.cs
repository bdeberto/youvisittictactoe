﻿

[System.Serializable]
public struct GameParameters
{
	public UnityEngine.Vector2 anchorPoint;
	public float width;
	public float height;
	public int gridSize;
    public int numPlayers;
}
