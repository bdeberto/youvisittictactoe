﻿public delegate void Interaction(int index);

public interface IInteractive
{
	Interaction InteractCallback { set; }
	int Index { set; }
	void SetDimensions(float width, float height);
	void SetPosition(UnityEngine.Vector2 position);
}
