﻿
[System.Serializable]
public class MatchLineGridEvaluator : GridEvaluator
{
    /// <summary>
    /// Assumption is that a win here == line length must match grid size
    /// </summary>
    protected override void Evaluate()
    {
        int baseLeftIndex = grid[0];
        int baseRightIndex = grid[parameters.gridSize - 1];
        int diagLeftRun = 0;
        int diagRightRun = 0;
        bool blankFound = false;
        for (int i = 0; i < parameters.gridSize; ++i)
        {
            for (int j = 0; !blankFound && j < parameters.gridSize; ++j)
            {
                blankFound = grid[i * parameters.gridSize + j] == GridUpdate.Blank;
            }
			//check vertical column
            int verticalRunningIndex = grid[i];
            int verticalRun = 1;
            for (int j = 1;
                j < parameters.gridSize
                && (grid[j * parameters.gridSize + i] == verticalRunningIndex &&
                    grid[j * parameters.gridSize + i] != GridUpdate.Blank);
                    ++j)
            {
                ++verticalRun;
            }
            //check horizontal rows
            int horizontalRun = 1;
            int runningRowIndex = grid[i * parameters.gridSize];
            for (int j = 1;
                j < parameters.gridSize &&
                (grid[i * parameters.gridSize + j] == runningRowIndex &&
                    grid[i * parameters.gridSize + j] != GridUpdate.Blank);
                    ++j)
            {
                ++horizontalRun;
            }
            //check diagonal from left
            int diagLeftTarget = grid[i + (parameters.gridSize * i)];
            if (diagLeftTarget == baseLeftIndex && diagLeftTarget != GridUpdate.Blank)
            {
                ++diagLeftRun;
            }
            //check diagonalfrom right
            int diagRightTarget = grid[parameters.gridSize - 1 - i + (parameters.gridSize * i)];
            if (diagRightTarget == baseRightIndex && diagRightTarget != GridUpdate.Blank)
            {
                ++diagRightRun;
            }
            //did someone win?
            if (verticalRun == parameters.gridSize ||
                diagLeftRun == parameters.gridSize ||
				diagRightRun == parameters.gridSize ||
                horizontalRun == parameters.gridSize)
            {
                canMove = false;
                updateWinSubscribers(currentTurn);
            }
        }
        //stalemate
        if (canMove && !blankFound)
        {
            canMove = false;
			updateWinSubscribers(GridUpdate.Blank);
        }
    }

    protected override void NextTurn()
    {
        currentTurn = ++currentTurn % parameters.numPlayers;
        if (updateTurnSubscribers != null)
        {
            updateTurnSubscribers(currentTurn);
        }
    }
}
