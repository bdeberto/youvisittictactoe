﻿using UnityEngine;

public delegate void GridUpdateCallback(GridUpdate parameters);
public delegate void IndexCallback(int index);

[System.Serializable]
public abstract class GridEvaluator : IEvaluationModule
{
    [SerializeField]
    protected GameParameters parameters;

    protected GridUpdateCallback updateGridSubscribers;
    protected IndexCallback updateTurnSubscribers;
    protected IndexCallback updateWinSubscribers;
    protected int[] grid;
    protected int currentTurn;
    protected bool canMove;

    public GridUpdateCallback RegisterForGridUpdates
    {
        set
        {
            updateGridSubscribers -= value;
            updateGridSubscribers += value;
        }
    }

    public IndexCallback RegisterForTurnUpdates
    {
        set
        {
            updateTurnSubscribers -= value;
            updateTurnSubscribers += value;
        }
    }

    public IndexCallback RegisterForWinUpdates
    {
        set
        {
            updateWinSubscribers -= value;
            updateWinSubscribers += value;
        }
    }

    public GameParameters Parameters
    {
        get
        {
            return parameters;
        }
    }

    public void Init()
    {
        //assume square grid
        grid = new int[(int)Mathf.Pow(parameters.gridSize, 2)];
        for (int i = 0; i < grid.Length; ++i)
        {
            grid[i] = GridUpdate.Blank;
        }
        currentTurn = 0;
        canMove = true;
    }

    protected abstract void Evaluate();

    protected abstract void NextTurn();

    #region IEvaluationModule

    public void Select(int index)
    {
		if (canMove && grid[index] == GridUpdate.Blank)
		{
			grid[index] = currentTurn;
			GridUpdate updateStruct = new GridUpdate();
			updateStruct.x = (int)(index / parameters.gridSize);
			updateStruct.y = (int)(index % parameters.gridSize);
			updateStruct.move = currentTurn;
			updateGridSubscribers(updateStruct);
			Evaluate();
			if (canMove)
			{
				NextTurn();
			}
		}
    }

    public virtual void Clear()
    {
        canMove = true;
		currentTurn = 0;
        updateTurnSubscribers(0);
        GridUpdate updateStruct = new GridUpdate();
        updateStruct.move = GridUpdate.Blank;
        int gridIndex = 0;
        for (int i = 0; i < parameters.gridSize; ++i)
        {
            for (int j = 0; j < parameters.gridSize; ++j, ++gridIndex)
            {
                grid[gridIndex] = GridUpdate.Blank;
                updateStruct.x = i;
                updateStruct.y = j;
                updateGridSubscribers(updateStruct);
            }
        }
    }

    #endregion
}
