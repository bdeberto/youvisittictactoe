﻿using UnityEngine;

public delegate void DimensionPass(Vector2 position, Vector2 size);

public interface IPresentationGenerator
{
	void GeneratePresentation(GameParameters parameters);
	void ClearPresentation();
	DimensionPass GenerateInteractionPoint { set; }
}
