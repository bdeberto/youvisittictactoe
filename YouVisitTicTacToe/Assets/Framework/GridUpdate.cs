﻿

public struct GridUpdate
{
    public const int Blank = -1;

    public int x;
    public int y;
    public int move;
}
